const Product = require("../models/Product");
const auth = require("../auth");

module.exports.createProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(userData);
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    stocks: req.body.stocks,
    isActive: req.body.isActive,
  });

  return Product.find({ name: req.body.name }).then((result) => {
    if (result.length > 0) {
      return res.send({ message: "Oh, no. Product already exixts." });
    } else {
      userData.isAdmin
        ? newProduct
            .save()
            .then((product) => {
              res.send({
                message: `You have successfully added product ${product.name}.`,
              });
            })
            .catch((err) => res.send(false))
        : res.status(401).send("You don't have access to this page.");
    }
  });
};

module.exports.getAllProducts = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return Product.find({}, { orders: 0, _id: 0 })
      .sort({ createdOn: -1 })
      .then((result) => res.send(result));
  } else {
    return res.status(401).send("You don't have access to the page!");
  }
};

module.exports.getAllActiveProducts = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  return Product.find({ isActive: true }, { orders: 0, _id: 0 })
    .sort({ createdOn: -1 })
    .then((result) => {
      if (result.length <= 0) {
        res.send({ message: "Oops. No product found." });
      } else {
        res.send(result);
      }
    });
};

module.exports.getSpecificProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(req.params.productId);
  return Product.findById(req.params.productId, {
    orders: 0,
    createdOn: 0,
    _id: 0,
  })
    .then((result) =>
      !result ? res.send({ message: "No product found." }) : res.send(result)
    )
    .catch((err) => {
      res.status(401).send({ message: "Oh no, Product does not exists." });
    });
};

module.exports.updateSpecificProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    let updateProduct = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      stocks: req.body.stocks,
    };

    return Product.findByIdAndUpdate(req.params.productId, updateProduct, {
      new: true,
    })
      .then((result) => {
        console.log(result);
        res.send({
          message: `Successfully updated to ${result.name}`,
        });
      })
      .catch((err) => {
        res.status(401).send({ message: "Sorry product was not found." });
      });
  } else {
    return res
      .status(401)
      .send({ message: "You don't have access to this page." });
  }
};

module.exports.archiveProduct = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let productName = await Product.findById(req.params.productId).then(
    (result) => result.name
  );
  let isActive = await Product.findById(req.params.productId).then(
    (result) => result.isActive
  );

  if (userData.isAdmin) {
    let updateIsActiveField = {
      isActive: req.body.isActive,
    };

    return Product.findByIdAndUpdate(
      req.params.productId,
      updateIsActiveField,
      {
        new: true,
      }
    )
      .then((result) => {
        if (isActive) {
          console.log(result);
          res.send({ message: `${productName} was removed from the archive.` });
        } else {
          console.log(result);
          res.send({ message: `${productName} was added to the archive.` });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(401).send({
          message:
            "Oops. Looks like the product that you are updating does not exists.",
        });
      });
  } else {
    return res
      .status(401)
      .send({ message: "You don't have access to this page." });
  }
};

module.exports.getOrders = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return Product.find({}, { orders: 1, _id: 0 }).then((result) =>
      !result ? res.send("No order found") : res.send(result)
    );
  } else {
    return res.status(401).send("You don't have access to the page!");
  }
};
