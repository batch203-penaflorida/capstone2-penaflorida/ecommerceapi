const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { request } = require("express");

module.exports.checkEmailExists = (req, res) => {
  return User.find({ email: req.body.email }).then((result) => {
    console.log(result);
    if (result.length > 0) {
      return res.send("User already exists!");
    } else {
      return res.send("No duplicate found!");
    }
  });
};

module.exports.updateAdminStatus = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let userName = await User.findById(req.params.userId).then(
    (result) => result.firstName
  );
  let isAdmin = await User.findById(req.params.userId).then(
    (result) => result.isAdmin
  );
  if (userData.isAdmin) {
    let updateIsActiveField = {
      isAdmin: req.body.isAdmin,
    };

    return User.findByIdAndUpdate(req.params.userId, updateIsActiveField, {
      new: true,
    })
      .then((result) => {
        if (isAdmin) {
          console.log(result);
          res.send({ message: `${userName} role is now an admin.` });
        } else {
          console.log(result);
          res.send({ message: `${userName} role is now a customer.` });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(401).send({
          message:
            "Oops. Looks like the product that you are updating does not exists.",
        });
      });
  } else {
    return res
      .status(401)
      .send({ message: "You don't have access to this page." });
  }
};

module.exports.registerUser = (req, res) => {
  return User.find({ email: req.body.email }).then((result) => {
    if (result.length > 0) {
      return res.send("Oh no, Email or Mobile Number already exixts.");
    } else {
      let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo,
      });
      console.log(newUser);
      return newUser
        .save()
        .then((user) => {
          console.log(user);
          res.send({
            message: `Thank you for registering to Joe's Ecommerce ${user.firstName}!`,
          });
        })
        .catch((err) => {
          console.log(err);
          res.send(false);
        });
    }
  });
};

module.exports.getAllUsers = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return User.find({}, { orders: 0, password: 0, _id: 0 }).then((result) => {
      if (!result) {
        res.send("No record found.");
      } else {
        res.send(result);
      }
    });
  } else {
    return res.status(401).send("You don't have access to the page!");
  }
};
module.exports.getActiveUsers = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return User.find({ isAdmin: true }, { orders: 0, password: 0, _id: 0 })
      .select({ orders: 0 })
      .then((result) => {
        res.send(result);
      });
  } else {
    return res.status(401).send("You don't have access to this page!");
  }
};

module.exports.loginUser = (req, res) => {
  console.log(req.body);
  return User.findOne({ email: req.body.email }).then((result) => {
    if (result == null) {
      return res.send({ message: "No user found!" });
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      if (isPasswordCorrect) {
        if (result.isAdmin) {
          return res.send({
            message: `Welcome back! ${result.firstName} | Role: Admin`,
            accessToken: auth.createAccessToken(result),
          });
        } else {
          return res.send({
            message: `Welcome back! ${result.firstName} | Role: Customer`,
            accessToken: auth.createAccessToken(result),
          });
        }
      } else {
        return res.send({ message: "Incorrect password" });
      }
    }
  });
};

module.exports.checkOut = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  const prodId = req.body.products;

  const id = prodId.map(({ productId }) => productId);
  const quantity = prodId.map(({ quantity }) => quantity);
  let condition;

  let data = {
    userId: userData.id,
    email: userData.email,
  };
  if (userData.isAdmin) {
    res.send(`Orders are only permitted for customer users only.`);
  } else {
    for (let i = 0; i < id.length; i++) {
      let isUserUpdated = await User.findById(data.userId).then((user) => {
        user.orders.push({
          totalAmount: req.body.totalAmount,
          products: req.body.products,
        });
        return user
          .save()
          .then((result) => {
            console.log(result);
            return true;
          })
          .catch((err) => {
            console.log(err);
            return false;
          });
      });

      let isProductUpdated = await Product.findById(id[i]).then((product) => {
        product.orders.push({
          userId: data.userId,
          userEmail: data.email,
          quantity: quantity[i],
        });
        product.stocks -= quantity[i];
        return product
          .save()
          .then((result) => {
            console.log(result);
            return true;
          })
          .catch((err) => {
            console.log(err);
            return false;
          });
      });

      if (isUserUpdated && isProductUpdated) {
        condition = true;
      } else {
        condition = false;
      }
    }
    if (condition == true) {
      res.send(
        "Thank you for shopping at Joe's E-Commerce. We are happy to serve you."
      );
    } else {
      res.send("Sorry, your order submission failed.");
    }
  }
};

module.exports.getAllOrders = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  return User.findById(userData.id, { orders: 1, _id: 0 })
    .sort({ purchasedOn: -1 })
    .then((result) => {
      res.send(result);
    });
};
