const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-bootcamp.m9kt1ev.mongodb.net/batch203_ecommerce_api?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`API is now running online at port number ${port}.`)
);
