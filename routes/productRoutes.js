const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/", auth.verify, productControllers.createProduct);

router.get("/all", auth.verify, productControllers.getAllProducts);

router.get("/", productControllers.getAllActiveProducts);

router.get("/:productId", productControllers.getSpecificProduct);

router.put("/:productId", productControllers.updateSpecificProduct);

router.get("/orders/all", productControllers.getOrders);

router.patch(
  "/archive/:productId",
  auth.verify,
  productControllers.archiveProduct
);

module.exports = router;
