const express = require("express");
const auth = require("../auth");

const router = express.Router();

const userControllers = require("../controllers/userControllers");

router.post("/checkEmail", userControllers.checkEmailExists);
router.post("/register", userControllers.registerUser);
router.post("/login", userControllers.loginUser);

router.get("/all", auth.verify, userControllers.getAllUsers);

router.get("/", auth.verify, userControllers.getActiveUsers);

router.patch("/role/:userId", auth.verify, userControllers.updateAdminStatus);

router.post("/checkout", auth.verify, userControllers.checkOut);

router.get("/orders", auth.verify, userControllers.getAllOrders);

router.delete("/remove");

module.exports = router;
